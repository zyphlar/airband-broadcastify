
Based on these guides:

https://wiki.radioreference.com/index.php/Broadcastify/RTLSDR-Airband

https://github.com/szpajder/RTLSDR-Airband/wiki/Configuring-Icecast-outputs

https://icecast.org/docs/icecast-2.4.1/basic-setup.html

Files go in:

- `/usr/local/etc/rtl_airband.conf`
- `/etc/systemd/system/rtl_airband.service`

Installed on Nanopi with FriendlyElec's Ubuntu

Filesystem backup here: `afp://wb-backups@WillsCloud.local/home/rtl-airband-ksle-nanopi-2022-10-1.img`
